package com.recruit.entities;

import com.recruit.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */
@Entity
public class Rol extends BaseEntity {

    @Column
    private String rol;
    @Column
    private String descripcion;


}
