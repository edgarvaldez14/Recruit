package com.recruit.entities;

import com.recruit.core.BaseEntity;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */

@Entity
@Table(name = "USUARIO")
public class Usuario extends BaseEntity implements Serializable{

    @Column(name = "NOMBRE_USUARIO", unique = true)
    private String nombreUsuario;

    @Column(name = "CONTRASENIA")
    private String contrasenia;

    @Column(name = "ESTADO")
    private boolean estado;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "usuario")
    private Set<UsuariosRoles> usuariosRolesSet = new HashSet<>();





    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public boolean isEstado() {
        return estado;
    }

    public Set<UsuariosRoles> getUsuariosRolesSet() {
        return usuariosRolesSet;
    }

    public void setUsuariosRolesSet(Set<UsuariosRoles> usuariosRolesSet) {
        this.usuariosRolesSet = usuariosRolesSet;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
