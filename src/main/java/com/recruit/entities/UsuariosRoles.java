package com.recruit.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */
@Entity
public class UsuariosRoles implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USUARIO_ROL_ID")
    private Long usuarioRolId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nombreUsuario", referencedColumnName = "NOMBRE_USUARIO" ,nullable =false,unique = true)
    private Usuario usuario;

    @Column(name = "ROL", nullable = false, length = 45)
    private String rol;

    @ManyToMany(cascade=CascadeType.ALL)
    @JoinColumn(name = "DESCRIPCION", referencedColumnName = "ROL", insertable = false,updatable = false)
    private Set<Rol> usuariosRoles;


    public Long getUsuarioRolId() {
        return usuarioRolId;
    }

    public void setUsuarioRolId(Long usuarioRolId) {
        this.usuarioRolId = usuarioRolId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Set<Rol> getUsuariosRoles() {
        return usuariosRoles;
    }

    public void setUsuariosRoles(Set<Rol> usuariosRoles) {
        this.usuariosRoles = usuariosRoles;
    }
}
