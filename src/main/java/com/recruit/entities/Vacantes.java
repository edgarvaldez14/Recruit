package com.recruit.entities;

import com.recruit.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */
@Entity
@Table(name = "VACANTES")
public class Vacantes extends BaseEntity {

    @Column(name = "NOMBRE_VACANTES")
    private String nombreVacante;
    @Column(name = "EMPRESA")
    private String empresa;
    @Column(name = "LOCALIDAD")
    private String localidad;
    @Column(name = "PUESTO_VACANTE")
    private String puestoVacante;
    @Column(name = "ACTIVIDAD", length = 200)
    private String actividad;
    @Column(name = "AREA")
    private String area;
    @Column(name = "REQUISITOS", length = 250)
    private String requisitos;
    @Column(name = "SALARIO")
    private double salario;
    @Column(name = "HORARIO")
    private String horario;


    public String getNombreVacante() {
        return nombreVacante;
    }

    public void setNombreVacante(String nombreVacante) {
        this.nombreVacante = nombreVacante;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getPuestoVacante() {
        return puestoVacante;
    }

    public void setPuestoVacante(String puestoVacante) {
        this.puestoVacante = puestoVacante;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(String requisitos) {
        this.requisitos = requisitos;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }
}
