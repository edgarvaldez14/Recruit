package com.recruit.repostiories;

import com.recruit.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario,Long>{

    Usuario findByNombreUsuario(String nombreUsuario);
}
