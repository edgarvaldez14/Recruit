package com.recruit.repostiories;

import com.recruit.entities.Vacantes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */
@Repository
public interface VacanteRepository extends JpaRepository<Vacantes, Serializable> {
}
