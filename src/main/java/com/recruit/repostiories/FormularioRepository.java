package com.recruit.repostiories;

import com.recruit.entities.Formulario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * Created by @DanielQuirozV on 7/21/2017.
 */
@Repository
public interface FormularioRepository extends JpaRepository<Formulario, Serializable>{
}
