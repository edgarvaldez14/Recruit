package com.recruit.services;

import com.recruit.entities.Formulario;
import com.recruit.repostiories.FormularioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */
@Service
public class FormularioServiceImpl implements FormularioService {

    @Autowired
    private FormularioRepository formularioRepository;


    @Override
    public List<Formulario> listarFormulario() {
        return formularioRepository.findAll();
    }

    @Override
    public void guardarFormulario(Formulario formulario) {
        formularioRepository.save(formulario);

    }

    @Override
    public Formulario buscarFormularioPorId(Long id) {
        return formularioRepository.getOne(id);
    }
}