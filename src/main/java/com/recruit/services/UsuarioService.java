package com.recruit.services;

import com.recruit.entities.Usuario;
import com.recruit.entities.UsuariosRoles;
import com.recruit.repostiories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */

@Service("servicioUsuarios")
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    public BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        try {
            Usuario usuario = usuarioRepository.findByNombreUsuario(s);
            System.out.println("Contraseña: " + encoder().encode("abc123"));
            List<GrantedAuthority> listaAutorizaciones = autorizaciones(usuario.getUsuariosRolesSet());
            return autorizarUsuario(usuario, listaAutorizaciones);
        } catch (SQLException e) {
//            LOGGER.error("ERROR al obtener los permisos necesarios para el usuario: " + nombreUsuario);
            e.printStackTrace();
            return null;
        }
    }


    private User autorizarUsuario(Usuario usuario, List<GrantedAuthority> listaAutorizaciones) throws SQLException {
        return new User(usuario.getNombreUsuario(), usuario.getContrasenia(), usuario.isEstado(), true, true, true, listaAutorizaciones);
    }

    private ArrayList<GrantedAuthority> autorizaciones(Set<UsuariosRoles> usuariosRoles) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        usuariosRoles.forEach(listaRoles -> authorities.add(new SimpleGrantedAuthority(listaRoles.getRol())));
        return new ArrayList<GrantedAuthority>(authorities);
    }

}
