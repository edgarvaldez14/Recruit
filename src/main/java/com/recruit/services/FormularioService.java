package com.recruit.services;


import com.recruit.entities.Formulario;

import java.util.List;

/**
 * Created by @DanielQuirozV on 7/21/2017.
 */
public interface FormularioService {

    List<Formulario> listarFormulario();
        void guardarFormulario(Formulario formulario);

        Formulario buscarFormularioPorId(Long id);
}
