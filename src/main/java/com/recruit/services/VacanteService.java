package com.recruit.services;

import com.recruit.entities.Vacantes;

import java.util.List;

/**
 * Created by @DanielQuirozV on 7/21/2017.
 */
public interface VacanteService {

    List<Vacantes> listarVacantes();
    void guardarVacante(Vacantes vacantes);

    Vacantes buscarVacantePorId(Long id);
}
