package com.recruit.services;

import com.recruit.entities.Vacantes;
import com.recruit.repostiories.VacanteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */
@Service
public class VacantesServiceImpl implements VacanteService {

    @Autowired
    private VacanteRepository vacanteRepository;


    @Override
    public List<Vacantes> listarVacantes() {
        return vacanteRepository.findAll();
    }

    @Override
    public void guardarVacante(Vacantes vacantes) {
        vacanteRepository.save(vacantes);
    }

    @Override
    public Vacantes buscarVacantePorId(Long id) {
        return vacanteRepository.getOne(id);
    }
}
