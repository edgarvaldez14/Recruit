package com.recruit.controller;

import com.recruit.services.VacanteService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by @DanielQuirozV on 7/20/2017.
 */
@Controller
public class LoginController {

    private final Log LOGGER = LogFactory.getLog(LoginController.class);

    @Autowired
    private VacanteService vacanteService;

    @GetMapping(value = {"/", "/login"})
    public ModelAndView login(@RequestParam(name = "error", required = false) String error,
                              @RequestParam(name = "logout", required = false) String logout) {
        ModelAndView mav = new ModelAndView();
        LOGGER.info(":-METODO: login " + ":-PARAMETROS " + "error: " + error + " logout " + logout);
        mav.addObject("error", error);
        mav.addObject("logout", logout);
        mav.setViewName("login");
        return mav;
    }



    @GetMapping(value = "/inicio")
    public ModelAndView loginSuccess(){
        ModelAndView mav = new ModelAndView();
        User usuario = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        LOGGER.info(":-METODO: login ");
        mav.addObject("usuario",usuario.getUsername());
        mav.addObject("vacantes", vacanteService.listarVacantes());
        mav.setViewName("index");
        return mav;
    }

}
