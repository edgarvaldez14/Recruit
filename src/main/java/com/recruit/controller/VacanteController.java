package com.recruit.controller;

import com.recruit.entities.Vacantes;
import com.recruit.services.VacanteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by @DanielQuirozV on 7/21/2017.
 */
@Controller

public class VacanteController {

    @Autowired
    private VacanteService vacanteService;

    @GetMapping("/verVacante")
    public ModelAndView verVacante(@RequestParam(name = "id", required = false) Long
                                               id){
        ModelAndView modelAndView = new ModelAndView();
        Vacantes vacantes =  vacanteService.buscarVacantePorId(id);
        modelAndView.addObject("vacantes",vacantes);
        modelAndView.setViewName("verVacantes");
        return modelAndView;
    }



    @GetMapping("/formularioVacantes")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView vacanteForm() {
        ModelAndView modelAndView = new ModelAndView();
        Vacantes vacante = new Vacantes();
        modelAndView.addObject("vacante", vacante);
        modelAndView.setViewName("/formularioVacantes");
        return modelAndView;
    }

    @PostMapping("/agregarVacante")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView addVacante(@ModelAttribute(name = "vacante") Vacantes vacantes) {
        ModelAndView mav = new ModelAndView();
        vacanteService.guardarVacante(vacantes);
        mav.setViewName("redirect:/inicio");
        return mav;
    }
}
