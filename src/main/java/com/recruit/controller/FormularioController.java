package com.recruit.controller;

import com.recruit.entities.Formulario;
import com.recruit.services.FormularioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by @DanielQuirozV on 7/21/2017.
 */
@Controller
public class FormularioController {

    @Autowired
    private FormularioService formularioService;

    @GetMapping("/verFormulario")
    public ModelAndView verVacante(@RequestParam(name = "id", required = false) Long id) {
        ModelAndView modelAndView = new ModelAndView();
        Formulario formulario = formularioService.buscarFormularioPorId(id);
        modelAndView.addObject("formulario", formulario);
        modelAndView.setViewName("verFormulario");
        return modelAndView;
    }

    @GetMapping("/solicitudes")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ModelAndView verSolicitudesVacantes() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("solicitudes", formularioService.listarFormulario());
        mav.setViewName("solicitudes");
        return mav;
    }


    @GetMapping("/formulario")
    public ModelAndView vacanteForm() {
        ModelAndView modelAndView = new ModelAndView();
        Formulario formulario = new Formulario();
        modelAndView.addObject("formulario", formulario);
        modelAndView.setViewName("/formulario");
        return modelAndView;
    }


    @PostMapping("/agregarFormulario")
    public ModelAndView addVacante(@ModelAttribute(name = "formulario") Formulario formulario) {
        ModelAndView mav = new ModelAndView();
        formularioService.guardarFormulario(formulario);
        mav.setViewName("redirect:/inicio");
        return mav;
    }
}
